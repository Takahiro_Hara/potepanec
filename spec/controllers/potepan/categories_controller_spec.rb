require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product) }

    before do
      taxon.products << product
      get :show, params: { id: taxon.id }
    end

    it "正常なレスポンスを返す" do
      expect(response).to be_success
    end

    it "@taxonを適切に割り当てる" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "@productsを適切に割り当てる" do
      products = [product]
      expect(assigns(:products)).to eq products
    end

    it "@taxonomiesを適切に割り当てる" do
      taxonomies = [taxon.taxonomy]
      expect(assigns(:taxonomies)).to eq taxonomies
    end

    it 'showテンプレートをレンダリングする' do
      expect(response).to render_template :show
    end
  end
end
