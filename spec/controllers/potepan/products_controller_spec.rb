require "rails_helper"

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    before { get :show, params: { id: product.id } }

    it "正常なレスポンスを返す" do
      expect(response).to be_success
    end

    it "showテンプレートをレンダリングする" do
      expect(response).to render_template :show
    end

    it "@productを適切に割り当てる" do
      expect(assigns(:product)).to eq product
    end

    describe "@related_products" do
      context "関連商品の数が3個の場合" do
        let!(:three_related_products) { create_list(:product, 3, taxons: [taxon]) }

        before { get :show, params: { id: product.id } }

        it "3個の商品が割り当てられる" do
          expect(assigns(:related_products).count).to eq 3
        end
      end

      context "関連商品の数が4個の場合" do
        let!(:four_related_products) { create_list(:product, 4, taxons: [taxon]) }

        before { get :show, params: { id: product.id } }

        it "4個の商品が割り当てられる" do
          expect(assigns(:related_products).count).to eq 4
        end
      end

      context "関連商品の数が5個の場合" do
        let!(:five_related_products) { create_list(:product, 5, taxons: [taxon]) }

        before { get :show, params: { id: product.id } }

        it "4個の商品が割り当てられる" do
          expect(assigns(:related_products).count).to eq 4
        end
      end
    end
  end
end
