require "rails_helper"

RSpec.describe Spree::Product, type: :model do
  describe "scope" do
    describe "without_self_product" do
      subject { Spree::Product.without_self_product(product) }

      let!(:product) { create(:product) }

      it "取得した商品群の中に引数の商品が含まれない" do
        is_expected.not_to include product
      end
    end
  end

  describe "#related_products" do
    subject { product.related_products }

    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:another_taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let!(:unrelated_product) { create(:product, taxons: [another_taxon]) }

    it "レシーバの商品が属するtaxonに紐づく別の商品を取得する" do
      is_expected.to include related_product
    end

    it "レシーバの商品を取得しない" do
      is_expected.not_to include product
    end

    it "レシーバの商品が属するものと異なるtaxonに紐づく商品を取得しない" do
      is_expected.not_to include unrelated_product
    end
  end
end
