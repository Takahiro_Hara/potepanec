require "rails_helper"

RSpec.describe Spree::Taxon, type: :model do
  describe "#products_with_self_and_descendants" do
    subject { taxon.products_with_self_and_descendants }

    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:taxon_child) { create(:taxon, taxonomy: taxonomy) }
    let!(:taxon_groundchild) { create(:taxon, taxonomy: taxonomy) }
    let!(:another_taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product_child) { create(:product, taxons: [taxon_child]) }
    let!(:product_groundchild) { create(:product, taxons: [taxon_groundchild]) }
    let!(:unrelated_product) { create(:product, taxons: [another_taxon]) }

    before do
      taxon_child.move_to_child_of(taxon)
      taxon_groundchild.move_to_child_of(taxon_child)
    end

    it "レシーバ及びその子孫taxonに紐づく商品を取得する" do
      is_expected.to include(product, product_child, product_groundchild)
    end

    it "レシーバ及びその子孫taxonと異なるtaxonに紐づく商品を取得しない" do
      is_expected.not_to include(unrelated_product)
    end
  end
end
