require "rails_helper"

RSpec.feature "Products", type: :feature do
  given!(:taxon) { create(:taxon) }
  given!(:product) { create(:product, taxons: [taxon]) }
  given!(:variant) { create(:variant, product: product) }
  given!(:related_product) { create(:product, taxons: [taxon], name: "related product") }

  scenario "商品の詳細と関連商品を表示する" do
    visit "potepan/products/#{product.id}"
    expect(page).to have_content "#{product.name}"
    expect(page).to have_content "#{product.display_price}"
    expect(page).to have_content "#{product.description}"
    expect(page).to have_content "#{variant.options_text}"
    expect(page).to have_selector "img[src=\"#{product.display_image.attachment.url(:large)}\"]"
    expect(page).to have_selector "img[src=\"#{product.display_image.attachment.url(:small)}\"]"
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_link "related product", href: "/potepan/products/#{related_product.id}"
    expect(page).to have_content "#{related_product.display_price}"
    expect(page).to have_selector "img[src=\"#{related_product.display_image.attachment.url}\"]"
  end

  scenario "関連商品のリンクから関連商品の詳細ページへ遷移する" do
    visit "potepan/products/#{product.id}"
    click_link "related product"
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
