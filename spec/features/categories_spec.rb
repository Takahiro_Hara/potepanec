require "rails_helper"

RSpec.feature "Categories", type: :feature do
  given!(:taxonomy) { create(:taxonomy) }
  given!(:taxon) { taxonomy.root }
  given!(:taxon_child) { create(:taxon, parent_id: taxon.id, taxonomy: taxonomy) }
  given!(:product) { create(:product, taxons: [taxon]) }
  given!(:product_child) { create(:product, taxons: [taxon_child]) }

  scenario "選択したカテゴリーの商品を画面に表示する" do
    visit "potepan/categories/#{taxon.id}"
    expect(page).to have_link "#{product.name}", href: "/potepan/products/#{product.id}"
    expect(page).to have_content "#{product.display_price}"
    expect(page).to have_selector "img[src=\"#{product.display_image.attachment.url}\"]"
    expect(page).to have_link "#{product_child.name}", href: "/potepan/products/#{product_child.id}"
    expect(page).to have_content "#{product_child.display_price}"
    expect(page).to have_selector "img[src=\"#{product_child.display_image.attachment.url}\"]"
    expect(page).to have_link(
      "#{taxon_child.name} (#{taxon_child.products_with_self_and_descendants.count})",
      href: "/potepan/categories/#{taxon_child.id}"
    )
  end

  scenario "カテゴリーのリンクから当該カテゴリーの商品一覧ページへ遷移する" do
    visit "potepan/categories/#{taxon.id}"
    click_link "#{taxon_child.name} (#{taxon_child.products_with_self_and_descendants.count})"
    expect(current_path).to eq potepan_category_path(taxon_child.id)
  end

  scenario "商品のリンクから当該商品の詳細ページへ遷移する" do
    visit "potepan/categories/#{taxon.id}"
    click_link "#{product.name}"
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
