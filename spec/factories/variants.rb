FactoryGirl.define do
  factory :variant, class: "Spree::Variant" do
    is_master false
    cost_price 0.15e2
    association :product
    after(:create) do |variant|
      variant.option_values << create(:option_value)
      variant.images << create(:image)
    end
  end
end
