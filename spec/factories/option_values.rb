FactoryGirl.define do
  factory :option_value, class: "Spree::OptionValue" do
    name "Small"
    presentation "S"
    position 1
    association :option_type
  end
end
