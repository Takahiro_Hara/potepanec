FactoryGirl.define do
  factory :taxonomy, class: "Spree::Taxonomy" do
    name "Categories"
  end
end
