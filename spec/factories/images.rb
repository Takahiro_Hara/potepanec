FactoryGirl.define do
  factory :image, class: "Spree::Image" do
    attachment_content_type "image/jpeg"
    sequence(:attachment_file_name) { |n| "test#{n}.jpeg" }
  end
end
