FactoryGirl.define do
  factory :product, class: "Spree::Product" do
    sequence(:name) { |n| "product name #{n}" }
    description "product description"
    sequence(:price) { |n| "#{n}e2" }
    association :shipping_category
    after(:create) do |product|
      product.images << create(:image)
    end
  end
end
