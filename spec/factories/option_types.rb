FactoryGirl.define do
  factory :option_type, class: "Spree::OptionType" do
    name "tshirt-size"
    presentation "Size"
    position 1
  end
end
