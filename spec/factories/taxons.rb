FactoryGirl.define do
  factory :taxon, class: "Spree::Taxon" do
    name "Bags"
    parent_id nil
    association :taxonomy
  end
end
