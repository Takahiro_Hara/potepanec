class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.products_with_self_and_descendants
    @taxonomies = Spree::Taxonomy.all.includes(:root)
  end
end
