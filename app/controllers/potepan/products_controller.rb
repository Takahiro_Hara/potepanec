class Potepan::ProductsController < ApplicationController
  NUM_RELATED_PRODUCTS_TO_DISPLAY = 4

  def show
    @product = Spree::Product.find(params[:id])
    @product_properties = @product.product_properties.includes(:property)
    @variant = @product.variants.find_by(id: params[:variant_id])
    product_or_variant = @variant || @product
    @images = product_or_variant.images
    @related_products = @product.related_products.sample(NUM_RELATED_PRODUCTS_TO_DISPLAY)
  end
end
