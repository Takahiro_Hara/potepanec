Spree::Taxon.class_eval do
  def products_with_self_and_descendants
    scope = Spree::Product.includes(:taxons, master: [:images, :default_price])
    scope.where(
      spree_taxons: { id: self_and_descendants.pluck(:id) }
    )
  end
end
