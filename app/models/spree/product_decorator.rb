Spree::Product.class_eval do
  MAX_PRODUCTS_TO_FETCH = 20

  scope :without_self_product, ->(product) { where.not(id: product.id) }
  scope :includes_image_and_price, -> { includes(master: [:images, :default_price]) }

  def related_products
    self.class.joins(:taxons).
      includes_image_and_price.
      where(spree_taxons: { id: taxons.ids }).
      without_self_product(self).
      distinct.
      limit(MAX_PRODUCTS_TO_FETCH)
  end
end
