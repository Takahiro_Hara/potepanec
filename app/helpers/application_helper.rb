module ApplicationHelper
  def full_title(page_title: "")
    base_title = "BIGBAG Store"
    if page_title.empty?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end

  def set_class_for_menu(menu_title)
    menu_title == fetch_active_dropdown_by_controller ? "active" : ""
  end

  def fetch_active_dropdown_by_controller
    case params[:controller]
    when "potepan/products", "potepan/categories"
      :shop
    when "potepan/sample"
      fetch_active_dropdown_by_action params[:action]
    else
      :home
    end
  end

  def fetch_active_dropdown_by_action(action)
    if action == "index"
      :home
    elsif action =~ /blog_/
      :blog
    elsif action =~ /checkout_/
      ""
    elsif action.in?(["about_us", "tokushoho", "privacy_policy"])
      :pages
    else
      :shop
    end
  end
end
